# Pinephone Wifi

A Wifi utility for the Pinephone and other Linux mobile devices using Network Manager

## Requirements

The only dependencies are tcl, tk, wl_clipboard (optional, see below) and sudo.

## Operations

- Scans for and lists the available wifi networks and shows the current network addresses

    Known SSIDs are shown in blue
 
	An active connection is shown in green 
 
- Connect to a wifi network \*

	Asks for a password/phrase if unknown \*
 
- Option to restart the wifi network if any problems are detected

- Note that pinephone_wifi assumes that the permanant mac address is the correct, required, address, and will reset the mac address if it differs.

## What works

Mostly everything should work as expected

## \* What does not work

QR Codes are available, if wl-clipboard is installed, to connect to a listed network or to save an SSID for future use.

As of writing Megapixels appears to be able to scan a Wifi QR Code and correctly interpret it. However outputting a URI or Copying the code to the clipboard does not seem to work at present.

If Copy works for you then the QR Code option in the network list should save a Wifi SSID for future use. In the same way the QR Code button in the password screen should add the password for the selected SSID, save the Wifi SSID and make the connection, unless the QR Code scanned does not match the selected SSID name.

If Megapixels outputs a URI correctly then some additional code will need to be added to take advantage of that.

## Screenshots

![List](screenshots/list.png) ![Password](screenshots/password.png)

## Installaton

Download the source code and run install.sh to:

	Copy the pinephone_wifi programme to /usr/bin

	Copy the icons to /usr/share/pixmaps/pinephone_wifi

	Copy the desktop file to /usr/share/applications


To update: download the source code and run install.sh again.

To uninstall Pinephone Wifi run uninstall.sh from the downloads directory.

This is a pre-release of the software. If you find it useful, good. If you do not then please file a bug report.
