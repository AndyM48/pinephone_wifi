#! /bin/sh

sudo cp pinephone_wifi /usr/bin
sudo chmod 0755 /usr/bin/pinephone_wifi
sudo mkdir -p /usr/share/pixmaps/pinephone_wifi
sudo cp icons/* /usr/share/pixmaps/pinephone_wifi
sudo cp pinephone_wifi.desktop /usr/share/applications

echo -e "\nInstall done\n"
